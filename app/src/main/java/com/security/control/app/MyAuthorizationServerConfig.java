package com.security.control.app;

import com.security.control.core.properties.OAuth2ClientProperties;
import com.security.control.core.properties.OAuth2Properties;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author imoot@gamil.com
 * @date 2019/1/10 0010 14:09
 * ----授权服务器配置
 */
@Configuration
@EnableAuthorizationServer
public class MyAuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private OAuth2Properties oAuth2Properties;

    @Autowired
    private TokenStore redisTokenStore;

    @Autowired(required = false)
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @Autowired(required = false)
    private TokenEnhancer jwtTokenEnhancer;

    //因为SpringBoot2.x以上版本的AuthenticationManager自动注入的配置发生了变化，所以使用构造函数注入AuthenticationConfiguration后来获取AuthenticationManager
    private final AuthenticationManager authenticationManager;

    public MyAuthorizationServerConfig(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        this.authenticationManager = authenticationConfiguration.getAuthenticationManager();
    }

    /**
     * @param endpoints
     * @throws Exception
     * 配置指向的端点
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(redisTokenStore)//配置token的存取器
                .authenticationManager(authenticationManager)//配置权限管理器
                .userDetailsService(userDetailsService);//配置用户信息处理类
        if (null != jwtAccessTokenConverter && null != jwtTokenEnhancer) {
            //创建链用来加入jwt的扩展类和jwt的转换类
            TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
            List<TokenEnhancer> enhancers = new ArrayList<>();
            enhancers.add(jwtTokenEnhancer);
            enhancers.add(jwtAccessTokenConverter);
            //把添加后的list放入链中
            enhancerChain.setTokenEnhancers(enhancers);

            endpoints
                    .tokenEnhancer(enhancerChain)//配置jwt的扩展器链
                    .accessTokenConverter(jwtAccessTokenConverter);//配置基于jwt的令牌转换器
        }
    }

    /**
     * @param clients
     * @throws Exception
     * 客户端相关配置
     * 客户端------>指认证服务器会给哪些第三方应用发放令牌
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        InMemoryClientDetailsServiceBuilder builder = clients.inMemory();//把客户端信息存在内存中
        if (CollectionUtils.isNotEmpty(oAuth2Properties.getClients())) {
            for (OAuth2ClientProperties client : oAuth2Properties.getClients()) {
                builder.withClient(client.getClientId())//客户端id
                        .secret(bCryptPasswordEncoder.encode(client.getClientSecret()))//客户端secret（注：SpringBoot2.x以上版本必须使用PasswordEncoder加密后设置在这里）
                        .accessTokenValiditySeconds(client.getAccessTokenValidateSeconds())//令牌的有效期
                        .refreshTokenValiditySeconds(21600)
                        .authorizedGrantTypes("refresh_token", "password")//配置这个客户端可以使用的授权模式
                        .scopes("all", "read", "write");//权限范围
            }
        }
    }
}

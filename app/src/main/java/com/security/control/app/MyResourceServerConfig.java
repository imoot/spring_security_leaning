package com.security.control.app;

import com.security.control.app.social.openid.OpenIdAuthenticationSecurityConfig;
import com.security.control.core.authentication.mobile.SmsValidateCodeAuthenticationSecurityConfig;
import com.security.control.core.authorize.AuthorizeConfigManager;
import com.security.control.core.properties.SecurityProperties;
import com.security.control.core.support.SecurityConstants;
import com.security.control.core.validate.config.ValidateCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * @author imoot@gamil.com
 * @date 2019/1/14 0014 16:51
 * -----资源服务器配置
 */
@Configuration
@EnableResourceServer
public class MyResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private AuthenticationSuccessHandler myAuthenticationSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler myAuthenticationFailureHandler;

    @Autowired
    private SmsValidateCodeAuthenticationSecurityConfig smsValidateCodeAuthenticationSecurityConfig;

    @Autowired
    private SpringSocialConfigurer mySocialSecurityConfig;

    @Autowired
    private ValidateCodeSecurityConfig validateCodeSecurityConfig;

    @Autowired
    private OpenIdAuthenticationSecurityConfig openIdAuthenticationSecurityConfig;

    @Autowired
    private AuthorizeConfigManager authorizeConfigManager;

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
                .loginProcessingUrl(SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_FORM)
                .successHandler(myAuthenticationSuccessHandler)
                .failureHandler(myAuthenticationFailureHandler);

        http
                .apply(validateCodeSecurityConfig)//把验证码校验配置在security过滤器链上
                .and()
                .apply(smsValidateCodeAuthenticationSecurityConfig)//把短信校验配置在security过滤器链上
                .and()
                .apply(mySocialSecurityConfig)//把social过滤器配置在security过滤器链上
                .and()
                .apply(openIdAuthenticationSecurityConfig)//把openid相关处理类配置在security过滤器链上
                .and()
                /*.authorizeRequests()//以下是对请求的授权设置
                .antMatchers(
                        SecurityConstants.DEFAULT_UNAUTHENTICATION_URL,
                        securityProperties.getBrowserProperties().getSignInUrl(),
                        securityProperties.getBrowserProperties().getSignUpUrl(),
                        securityProperties.getBrowserProperties().getLogoutUrl(),
                        SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX + "/*",
                        SecurityConstants.DEFAULT_SOCIAL_AUTHENTICATION_URL + "/*",
                        securityProperties.getQqSocialProperties().getFilterProcessesUrl() + "/*",
                        SecurityConstants.DEFAULT_SIGN_UP_PAGE_URL,
                        SecurityConstants.DEFAULT_SESSION_INVALID_URL,
                        "/social/signUp",
                        "/user/register").permitAll()///添加匹配器，去掉访问url需要的权限
                .anyRequest()//任何请求
                .authenticated()//都需要身份认证
                .and()*/
                .csrf()//以下是对跨站请求伪造的设置
                .disable();//停用跨站请求伪造防御
        authorizeConfigManager.config(http.authorizeRequests());
    }
}

package com.security.control.app.exception;

/**
 * @author imoot@gamil.com
 * @date 2019/1/22 0022 16:44
 */
public class AppSecretException extends RuntimeException {

    public AppSecretException(String message) {
        super(message);
    }
}

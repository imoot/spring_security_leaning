package com.security.control.app.support;

import com.security.control.core.social.config.MySpringSocialConfigurer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/22 0022 16:51
 * spring中所有的bean初始化之前和之后都必须通过BeanPostProcessor
 * 在spring中的bean初始化之后做判断
 * 判断如果为自定义的SocialSecurityConfig，则修改注册地址
 */
@Component
public class SpringSocialConfigurerPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(StringUtils.equals(beanName,"mySocialSecurityConfig")){
            MySpringSocialConfigurer configurer = (MySpringSocialConfigurer) bean;
            configurer.signupUrl("/social/signUp");
            return configurer;
        }
        return bean;
    }
}

package com.security.control.core.social.qq.api;

/**
 * @author imoot@gamil.com
 * @date 2019/1/3 0003 15:04
 * ----qq相关操作接口
 */
public interface QQ {

    QQUserInfo getUserInfo();

}

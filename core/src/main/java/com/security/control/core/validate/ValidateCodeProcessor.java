package com.security.control.core.validate;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author imoot@gamil.com
 * @date 2018/12/21 0021 17:51
 * ---验证码处理器，封装了不同验证码的处理逻辑
 */
public interface ValidateCodeProcessor {

    /**
     * @param request
     * @throws Exception
     *创建验证码
     */
    void create(ServletWebRequest request) throws Exception;

    /**
     * @param request
     *校验验证码
     */
    void validate(ServletWebRequest request);
}

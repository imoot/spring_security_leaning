package com.security.control.core.social.wx.connect;

import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.social.oauth2.AccessGrant;

/**
 * @author imoot@gamil.com
 * @date 2019/1/8 0008 11:35
 * -----自定义访问授权类
 */
public class WxAccessGrant extends AccessGrant {

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    private String openId;

    public WxAccessGrant() {
        super("");
    }

    public WxAccessGrant(String accessToken, String scope, String refreshToken, Long expiresIn) {
        super(accessToken, scope, refreshToken, expiresIn);
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}

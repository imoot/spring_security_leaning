package com.security.control.core.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 11:48
 * ---自定义yml/properties文件读取类（ImageValidateCode）
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class ImageValidateCodeProperties extends ValidateCodeProperties {

    //图片宽度
    @Value("${com.security.control.validate.image.width}")
    private int width = 67;

    //图片高度
    @Value("${com.security.control.validate.image.height}")
    private int height = 23;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}

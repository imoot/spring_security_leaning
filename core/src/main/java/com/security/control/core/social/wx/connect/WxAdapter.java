package com.security.control.core.social.wx.connect;

import com.security.control.core.social.wx.api.Wx;
import com.security.control.core.social.wx.api.WxUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * @author imoot@gamil.com
 * @date 2019/1/4 0004 11:06
 * -----创建api与服务提供商的适配器
 */
public class WxAdapter implements ApiAdapter<Wx> {

    private String openId;

    public WxAdapter(String openId) {
        this.openId = openId;
    }

    public WxAdapter() {
    }

    //用来测试服务是否是通的
    @Override
    public boolean test(Wx wx) {
        return true;
    }

    //设置连接的属性值
    @Override
    public void setConnectionValues(Wx api, ConnectionValues connectionValues) {
        WxUserInfo userInfo = api.getUserInfo(openId);
        connectionValues.setDisplayName(userInfo.getNickname());//昵称
        connectionValues.setImageUrl(userInfo.getHeadimgurl());//头像
        connectionValues.setProfileUrl(null);//主页
        connectionValues.setProviderUserId(userInfo.getOpenid());//用户在服务商中的唯一id
    }

    @Override
    public UserProfile fetchUserProfile(Wx api) {
        return null;
    }

    @Override
    public void updateStatus(Wx wx, String s) {

    }
}

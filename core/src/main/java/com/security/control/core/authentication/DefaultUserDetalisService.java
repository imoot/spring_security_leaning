package com.security.control.core.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author imoot@gamil.com
 * @date 2019/1/14 0014 17:50
 * 默认的UserDetailsService实现
 * ---不做任何配置，只在控制台打印一句日志并抛出异常，提醒业务系统自己配置UserDetailsService
 */
public class DefaultUserDetalisService implements UserDetailsService {

    private Logger log = LoggerFactory.getLogger(DefaultUserDetalisService.class);

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.warn("请配置UserDetailsService接口的实现");
        throw new UsernameNotFoundException(username);
    }
}

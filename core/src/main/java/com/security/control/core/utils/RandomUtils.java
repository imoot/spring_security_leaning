package com.security.control.core.utils;

import java.util.Random;

/**
 * @author imoot@gamil.com
 * @date 2019/1/12 0012 16:38
 * 各种随机数
 */
public class RandomUtils {

    private static final Random RANDOM = new Random();

    public RandomUtils() {
    }

    /**
     * ASCII编码表中随机出count位
     */
    public static String randomAscii(int count) {
        return random(count, 32, 127, false, false);
    }

    /**
     * 指定最小长度minLengthInclusive，并指定最大长度maxLengthExclusive后从ASCII编码表中随机出count位
     */
    public static String randomAscii(int minLengthInclusive, int maxLengthExclusive) {
        return randomAscii(org.apache.commons.lang3.RandomUtils.nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * 26个英文字母大小写中随机出count位
     */
    public static String randomAlphabetic(int count) {
        return random(count, true, false);
    }

    /**
     * 指定最小长度minLengthInclusive，并指定最大长度maxLengthExclusive后从26个英文字母大小写中随机出count位
     */
    public static String randomAlphabetic(int minLengthInclusive, int maxLengthExclusive) {
        return randomAlphabetic(org.apache.commons.lang3.RandomUtils.nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * 26个英文字母大小写与阿拉伯数字中随机出count位
     */
    public static String randomAlphanumeric(int count) {
        return random(count, true, true);
    }

    /**
     * 指定最小长度minLengthInclusive，并指定最大长度maxLengthExclusive后从26个英文字母大小写与阿拉伯数字中随机出count位
     */
    public static String randomAlphanumeric(int minLengthInclusive, int maxLengthExclusive) {
        return randomAlphanumeric(org.apache.commons.lang3.RandomUtils.nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * 符号随机出count位
     */
    public static String randomGraph(int count) {
        return random(count, 33, 126, false, false);
    }

    /**
     * 指定最小长度minLengthInclusive，并指定最大长度maxLengthExclusive后从符号中随机出count位
     */
    public static String randomGraph(int minLengthInclusive, int maxLengthExclusive) {
        return randomGraph(org.apache.commons.lang3.RandomUtils.nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * 阿拉伯数字随机出count位
     */
    public static String randomNumeric(int count) {
        return random(count, false, true);
    }

    /**
     * 指定最小长度minLengthInclusive，并指定最大长度maxLengthExclusive后从阿拉伯数字中随机出count位
     */
    public static String randomNumeric(int minLengthInclusive, int maxLengthExclusive) {
        return randomNumeric(org.apache.commons.lang3.RandomUtils.nextInt(minLengthInclusive, maxLengthExclusive));
    }

    /**
     * 由numbers决定加不加数字，并由letters决定加不加字母随机出count位
     */
    public static String random(int count, boolean letters, boolean numbers) {
        return random(count, 0, 0, letters, numbers);
    }

    /**
     * 从start开始到end结束由numbers决定加不加数字，并由letters决定加不加字母随机出count位
     */
    public static String random(int count, int start, int end, boolean letters, boolean numbers) {
        return random(count, start, end, letters, numbers, (char[])null, RANDOM);
    }

    /**
     * 在指定的chars（字符集）中从start开始到end结束由numbers决定加不加数字，并由letters决定加不加字母随机出count位
     */
    public static String random(int count, int start, int end, boolean letters, boolean numbers, char... chars) {
        return random(count, start, end, letters, numbers, chars, RANDOM);
    }

    private static String random(int count, int start, int end, boolean letters, boolean numbers, char[] chars, Random random) {
        if (count == 0) {
            return "";
        } else if (count < 0) {
            throw new IllegalArgumentException("Requested random string length " + count + " is less than 0.");
        } else if (chars != null && chars.length == 0) {
            throw new IllegalArgumentException("The chars array must not be empty");
        } else {
            if (start == 0 && end == 0) {
                if (chars != null) {
                    end = chars.length;
                } else if (!letters && !numbers) {
                    end = 1114111;
                } else {
                    end = 123;
                    start = 32;
                }
            } else if (end <= start) {
                throw new IllegalArgumentException("Parameter end (" + end + ") must be greater than start (" + start + ")");
            }

            if (chars == null && (numbers && end <= 48 || letters && end <= 65)) {
                throw new IllegalArgumentException("Parameter end (" + end + ") must be greater then (" + 48 + ") for generating digits or greater then (" + 65 + ") for generating letters.");
            } else {
                StringBuilder builder = new StringBuilder(count);
                int gap = end - start;

                while(true) {
                    while(count-- != 0) {
                        int codePoint;
                        if (chars == null) {
                            codePoint = random.nextInt(gap) + start;
                            switch(Character.getType(codePoint)) {
                                case 0:
                                case 18:
                                case 19:
                                    ++count;
                                    continue;
                            }
                        } else {
                            codePoint = chars[random.nextInt(gap) + start];
                        }

                        int numberOfChars = Character.charCount(codePoint);
                        if (count == 0 && numberOfChars > 1) {
                            ++count;
                        } else if ((!letters || !Character.isLetter(codePoint)) && (!numbers || !Character.isDigit(codePoint)) && (letters || numbers)) {
                            ++count;
                        } else {
                            builder.appendCodePoint(codePoint);
                            if (numberOfChars == 2) {
                                --count;
                            }
                        }
                    }

                    return builder.toString();
                }
            }
        }
    }

    public static String random(int count, String chars) {
        return chars == null ? random(count, 0, 0, false, false, (char[])null, RANDOM) : random(count, chars.toCharArray());
    }

    public static String random(int count, char... chars) {
        return chars == null ? random(count, 0, 0, false, false, (char[])null, RANDOM) : random(count, 0, chars.length, false, false, chars, RANDOM);
    }

}

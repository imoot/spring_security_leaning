package com.security.control.core.validate.impl;

import com.security.control.core.validate.exception.ValidateCodeException;
import com.security.control.core.validate.support.ValidateCode;
import com.security.control.core.validate.support.ValidateCodeType;
import com.security.control.core.validate.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.util.Map;

/**
 * @author imoot@gamil.com
 * @date 2018/12/21 0021 17:55
 * ---验证码处理器的抽象实现
 */
public abstract class AbstractValidateCodeProcessor<C extends ValidateCode> implements ValidateCodeProcessor {

    private Logger log = LoggerFactory.getLogger(AbstractValidateCodeProcessor.class);

    //收集系统中所有的{@link ValidateCodeGenerator}接口的实现
    @Autowired
    private Map<String, ValidateCodeGenerator> validateCodeGenerators;

    //validatecode的保存、移除等操作类
    @Autowired
    private ValidateCodeRepository validateCodeRepository;

    /**
     * @param request
     * 创建验证码
     */
    @Override
    public void create(ServletWebRequest request) throws Exception {
        C validateCode = generate(request);
        save(request, validateCode);
        send(request, validateCode);
    }

    /**
     * @param request
     * @return C
     * 生成校验码
     */
    @SuppressWarnings("unchecked")
    private C generate(ServletWebRequest request) {
        String type = getValidateCodeType(request).toString().toLowerCase();
        String generatorName = type + ValidateCodeGenerator.class.getSimpleName();
        ValidateCodeGenerator validateCodeGenerator = validateCodeGenerators.get(generatorName);
        if (validateCodeGenerator == null) {
            throw new ValidateCodeException("验证码生成器" + generatorName + "不存在");
        }
        return (C) validateCodeGenerator.generate(request);
    }

    /**
     * @param request
     * @param validateCode
     * 保存校验码
     */
    private void save(ServletWebRequest request, C validateCode) {
        ValidateCode code = new ValidateCode(validateCode.getCode(), validateCode.getExpireTime());
        validateCodeRepository.save(request, code, getValidateCodeType(request));
    }

    /**
     * @param request
     * @param validateCode
     * @throws Exception
     * 发送校验码，由子类实现
     */
    protected abstract void send(ServletWebRequest request, C validateCode) throws Exception;

    /**
     * @param request
     * @return ValidateCodeType
     * 根据请求的url获取校验码类型
     */
    private ValidateCodeType getValidateCodeType(ServletWebRequest request) {
        String type = StringUtils.substringBefore(getClass().getSimpleName(), "ValidateCodeProcessor");
        return ValidateCodeType.valueOf(type.toUpperCase());
    }

    /**
     * @param request
     * 校验验证码
     */
    @SuppressWarnings("unchecked")
    @Override
    public void validate(ServletWebRequest request) {
        ValidateCodeType codeType = getValidateCodeType(request);
        C codeInSession = (C) validateCodeRepository.get(request, codeType);
        String codeInRequest;
        try {
            codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), codeType.getParamNameOnValidate());
        } catch (ServletRequestBindingException e) {
            log.error("获取验证码的值失败", e);
            throw new ValidateCodeException("获取验证码的值失败");
        }
        if (StringUtils.isBlank(codeInRequest)) {
            throw new ValidateCodeException(codeType + "请填写验证码");
        }
        if (codeInSession == null) {
            throw new ValidateCodeException(codeType + "验证码不存在");
        }
        if (codeInSession.isExpried()) {
            validateCodeRepository.remove(request, codeType);
            throw new ValidateCodeException(codeType + "验证码已过期，请重新获取");
        }
        if (!StringUtils.equals(codeInSession.getCode(), codeInRequest)) {
            throw new ValidateCodeException(codeType + "验证码不正确");
        }
        validateCodeRepository.remove(request, codeType);
    }
}

package com.security.control.core.social.qq.connect;

import com.security.control.core.social.qq.api.QQ;
import com.security.control.core.social.qq.api.QQUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * @author imoot@gamil.com
 * @date 2019/1/4 0004 11:06
 * -----创建api与服务提供商的适配器
 */
public class QQAdapter implements ApiAdapter<QQ> {

    //用来测试服务是否是通的
    @Override
    public boolean test(QQ qq) {
        return true;
    }

    //设置连接的属性值
    @Override
    public void setConnectionValues(QQ api, ConnectionValues connectionValues) {
        QQUserInfo userInfo = api.getUserInfo();
        connectionValues.setDisplayName(userInfo.getNickname());//昵称
        connectionValues.setImageUrl(userInfo.getFigureurl_qq_1());//头像
        connectionValues.setProfileUrl(null);//主页
        connectionValues.setProviderUserId(userInfo.getOpenId());//用户在服务商中的唯一id
    }

    @Override
    public UserProfile fetchUserProfile(QQ api) {
        return null;
    }

    @Override
    public void updateStatus(QQ qq, String s) {

    }
}

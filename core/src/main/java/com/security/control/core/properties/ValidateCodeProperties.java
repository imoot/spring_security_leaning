package com.security.control.core.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 11:53
 * ---自定义验证码配置（yml/properties读取）类
 */
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class ValidateCodeProperties {

    //验证码长度
    @Value("${com.security.control.validate.length}")
    private int length = 4;

    //验证码过期时间
    @Value("${com.security.control.validate.expireIn}")
    private int expireIn = 60;

    //拦截的url
    @Value("${com.security.control.validate.url}")
    private String url;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(int expireIn) {
        this.expireIn = expireIn;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

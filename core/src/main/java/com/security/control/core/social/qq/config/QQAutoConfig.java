package com.security.control.core.social.qq.config;

import com.security.control.core.properties.QQSocialProperties;
import com.security.control.core.properties.SecurityProperties;
import com.security.control.core.social.qq.connect.QQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;

/**
 * @author imoot@gamil.com
 * @date 2019/1/4 0004 16:01
 * -----在springboot2.x的版本开始移除了SocialAutoConfigurerAdapter，所以这里要继承SocialAutoConfigurerAdapter的父类SocialConfigurerAdapter
 */
@Configuration
@ConditionalOnProperty(prefix = "com.security.control.social.qq", name = "appId")//当yml中配置了appId时才启用
public class QQAutoConfig extends SocialConfigurerAdapter {

    @Autowired
    private SecurityProperties securityProperties;

    //添加连接工厂的数据库存储配置
    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer configurer, Environment environment) {
        configurer.addConnectionFactory(createConnectionFactory());
    }

    //用providerId、appId、appSecret构建QQ授权登录连接工厂
    public ConnectionFactory<?> createConnectionFactory() {
        QQSocialProperties qqSocialProperties = securityProperties.getQqSocialProperties();
        return new QQConnectionFactory(qqSocialProperties.getProviderId(), qqSocialProperties.getAppId(), qqSocialProperties.getAppSecret());
    }

    //获取数据库存储类
    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        return super.getUsersConnectionRepository(connectionFactoryLocator);
    }

}

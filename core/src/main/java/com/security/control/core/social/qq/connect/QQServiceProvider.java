package com.security.control.core.social.qq.connect;

import com.security.control.core.social.qq.api.QQ;
import com.security.control.core.social.qq.api.QQImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Template;

/**
 * @author imoot@gamil.com
 * @date 2019/1/3 0003 17:38
 * -----创建QQ服务提供商
 */
public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ> {

    private String appId;

    private static final String URL_AUTHORIZE = "https://graph.qq.com/oauth2.0/authorize";

    private static final String URL_ACCESS_TOKEN = "https://graph.qq.com/oauth2.0/token";

    //使用传入的appId、appSecret、授权地址、获取的access_token创建OAuth2模板对象后，调用父类方法创建OAuth2服务商实例
    public QQServiceProvider(String appId, String appSecret) {
        super(new QQOAuth2Template(appId, appSecret, URL_AUTHORIZE, URL_ACCESS_TOKEN));
        this.appId = appId;
    }

    //使用传入的access_token、appId创建api（即：自定义相关操作/逻辑类）实现类
    @Override
    public QQ getApi(String accessToken) {
        return new QQImpl(accessToken, appId);
    }
}

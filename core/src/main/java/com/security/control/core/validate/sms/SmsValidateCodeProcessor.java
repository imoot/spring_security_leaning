package com.security.control.core.validate.sms;

import com.security.control.core.support.SecurityConstants;
import com.security.control.core.validate.support.ValidateCode;
import com.security.control.core.validate.impl.AbstractValidateCodeProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author imoot@gamil.com
 * @date 2018/12/23 0023 10:31
 * ---短信验证码处理器（负责短信发送）
 */
@Component("smsValidateCodeProcessor")
public class SmsValidateCodeProcessor extends AbstractValidateCodeProcessor<ValidateCode> {

    //短信验证码发送类
    @Autowired
    private SmsValidateCodeSender smsValidateCodeSender;

    /**
     * @param request
     * @param validateCode
     * @throws Exception
     * 发送短信验证码
     */
    @Override
    protected void send(ServletWebRequest request, ValidateCode validateCode) throws Exception {
        String paramName = SecurityConstants.DEFAULT_PARAMETER_NAME_MOBILE;
        String mobile = ServletRequestUtils.getRequiredStringParameter(request.getRequest(), paramName);
        smsValidateCodeSender.send(mobile, validateCode.getCode());
    }
}

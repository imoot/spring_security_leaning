package com.security.control.core.social.qq.connect;

import com.security.control.core.social.qq.api.QQ;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;

/**
 * @author imoot@gamil.com
 * @date 2019/1/4 0004 11:19
 * -----创建连接工厂
 */
public class QQConnectionFactory extends OAuth2ConnectionFactory<QQ> {

    //使用传入的providerId、appId和appSecret实例化的服务商类、服务商与api（即：自定义相关操作/逻辑类）的适配器来调用父类构造函数创建该服务商的连接工厂
    public QQConnectionFactory(String providerId, String appId, String appSecret) {
        super(providerId, new QQServiceProvider(appId, appSecret), new QQAdapter());
    }
}

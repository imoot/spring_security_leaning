package com.security.control.core.validate.image;

import com.security.control.core.properties.SecurityProperties;
import com.security.control.core.validate.ValidateCodeGenerator;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 14:32
 * ---默认的图片验证码生成器
 */
public class ImageValidateCodeGenerator implements ValidateCodeGenerator {

    private SecurityProperties securityProperties;

    //生成图片验证码
    @Override
    public ImageValidateCode generate(ServletWebRequest request) {
        //判断请求中是否传入图片的宽度和高度，如果没有，则使用配置文件中的宽度和高度值
        int width = ServletRequestUtils.getIntParameter(request.getRequest(), "width", securityProperties.getImageValidateCodeProperties().getWidth());
        int height = ServletRequestUtils.getIntParameter(request.getRequest(), "height", securityProperties.getImageValidateCodeProperties().getHeight());
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        Graphics g = image.getGraphics();

        Random random = new Random();

        //生成带条纹的背景图片
        g.setColor(getRandColor(200, 250));
        g.fillRect(0, 0, width, height);
        g.setFont(new Font("Times New Roman", Font.ITALIC, 20));
        g.setColor(getRandColor(160, 200));
        for (int i = 0; i < 155; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            g.drawLine(x, y, x + xl, y + yl);
        }

        //根据配置文件生成相应长度的随机数并写到图片上
        String sRand = "";
        for (int i = 0; i < securityProperties.getImageValidateCodeProperties().getLength(); i++) {
            String rand = String.valueOf(random.nextInt(10));
            sRand += rand;
            g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));
            g.drawString(rand, 13 * i + 6, 16);
        }

        g.dispose();

        return new ImageValidateCode(image, sRand, securityProperties.getImageValidateCodeProperties().getExpireIn());
    }

    //生成条纹背景
    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255) {
            fc = 255;
        }
        if (bc > 255) {
            bc = 255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }

    public SecurityProperties getSecurityProperties() {
        return securityProperties;
    }

    public void setSecurityProperties(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }
}

package com.security.control.core.validate.image;

import com.security.control.core.validate.support.ValidateCode;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 10:21
 * ----图片验证码
 */
public class ImageValidateCode extends ValidateCode {

    private static final long serialVersionUID = -6020470039852318468L;

    private BufferedImage image;//图片

    public ImageValidateCode(BufferedImage image, String code, int expireTime) {
        super(code,expireTime);
        this.image = image;
    }

    public ImageValidateCode(BufferedImage image, String code, LocalDateTime expireTime) {
        super(code,expireTime);
        this.image = image;
    }

    public ImageValidateCode() {}

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
}

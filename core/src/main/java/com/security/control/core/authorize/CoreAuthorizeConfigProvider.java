package com.security.control.core.authorize;

import com.security.control.core.properties.SecurityProperties;
import com.security.control.core.support.SecurityConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/15 0015 10:19
 * 核心模块的授权配置提供器，用来配置安全模块涉及到的url的授权
 */
@Component
@Order(Integer.MIN_VALUE)
public class CoreAuthorizeConfigProvider implements AuthorizeConfigProvider {

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        config.antMatchers(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL,
                SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_MOBILE,
                SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_OPENID,
                SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX + "/*",
                securityProperties.getBrowserProperties().getSignInUrl(),
                securityProperties.getBrowserProperties().getSignUpUrl(),
                securityProperties.getSessionProperties().getInvalidSessionUrl()).permitAll();

        if (StringUtils.isNotBlank(securityProperties.getBrowserProperties().getLogoutUrl())) {
            config.antMatchers(securityProperties.getBrowserProperties().getLogoutUrl()).permitAll();
        }
        return false;
    }
}

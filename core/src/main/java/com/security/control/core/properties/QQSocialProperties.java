package com.security.control.core.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/4 0004 15:32
 * -----springboot2.x开始移除了SocialProperties，所以需要自定义
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class QQSocialProperties extends SocialProperties {

    @Value("${com.security.control.social.qq.providerId}")
    private String providerId = "qq";

    @Value("${com.security.control.social.qq.appId}")
    private String appId;

    @Value("${com.security.control.social.qq.appSecret}")
    private String appSecret;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}

package com.security.control.core.validate;

import com.security.control.core.validate.exception.ValidateCodeException;
import com.security.control.core.validate.support.ValidateCodeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author imoot@gamil.com
 * @date 2018/12/23 0023 9:33
 * ---验证码处理器的管理器
 */
@Component
public class ValidateCodeProcessorHolder {

    //使用spring的依赖搜索，自动寻找ValidateCodeProcessor的实现类并以<类名,ValidateCodeProcessor实现类>的方式放入map中
    @Autowired
    private Map<String, ValidateCodeProcessor> validateCodeProcessors;

    public ValidateCodeProcessor findValidateCodeProcessor(ValidateCodeType type) {
        return findValidateCodeProcessor(type.toString().toLowerCase());
    }

    public ValidateCodeProcessor findValidateCodeProcessor(String type) {
        String name = type.toLowerCase() + ValidateCodeProcessor.class.getSimpleName();
        ValidateCodeProcessor processor = validateCodeProcessors.get(name);
        if (processor == null) {
            throw new ValidateCodeException("验证码处理器" + name + "不存在");
        }
        return processor;
    }

}

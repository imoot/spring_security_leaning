package com.security.control.core.social.wx.connect;

import com.security.control.core.social.wx.api.Wx;
import com.security.control.core.social.wx.api.WxImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

/**
 * @author imoot@gamil.com
 * @date 2019/1/3 0003 17:38
 * -----创建wx服务提供商
 */
public class WxServiceProvider extends AbstractOAuth2ServiceProvider<Wx> {

    private static final String URL_AUTHORIZE = "https://open.weixin.qq.com/connect/qrconnect";

    private static final String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token";

    //使用传入的appId、appSecret、授权地址、获取的access_token创建OAuth2模板对象后，调用父类方法创建OAuth2服务商实例
    public WxServiceProvider(String appId, String appSecret) {
        super(new WxOAuth2Template(appId, appSecret, URL_AUTHORIZE, URL_ACCESS_TOKEN));
    }

    //使用传入的access_toke创建api（即：自定义相关操作/逻辑类）实现类
    @Override
    public Wx getApi(String accessToken) {
        return new WxImpl(accessToken);
    }
}

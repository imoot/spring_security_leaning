package com.security.control.core.properties;

import com.security.control.core.support.AuthenticateResponseType;
import com.security.control.core.support.SecurityConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/7 0007 14:46
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class BrowserProperties {

    //使用@Value对应yml/properties中的key
    @Value("${com.security.control.browser.signInUrl}")
    private String signInUrl = SecurityConstants.DEFAULT_SIGN_IN_PAGE_URL;

    @Value("${com.security.control.browser.authenticateResponseType}")
    private AuthenticateResponseType authenticateResponseType = AuthenticateResponseType.JSON;

    @Value("${com.security.control.browser.rememberMeSeconds}")
    private int rememberMeSeconds = 3600;

    @Value("${com.security.control.browser.signUpUrl}")
    private String signUpUrl = SecurityConstants.DEFAULT_SIGN_UP_PAGE_URL;

    @Value("${com.security.control.browser.logoutUrl}")
    private String logoutUrl = SecurityConstants.DEFAULT_LOGOUT_PAGE_URL;

    @Value("${com.security.control.browser.signInSuccessUrl}")
    private String signInSuccessUrl;

    public String getSignInUrl() {
        return signInUrl;
    }

    public void setSignInUrl(String signInUrl) {
        this.signInUrl = signInUrl;
    }

    public AuthenticateResponseType getAuthenticateResponseType() {
        return authenticateResponseType;
    }

    public void setAuthenticateResponseType(AuthenticateResponseType authenticateResponseType) {
        this.authenticateResponseType = authenticateResponseType;
    }

    public int getRememberMeSeconds() {
        return rememberMeSeconds;
    }

    public void setRememberMeSeconds(int rememberMeSeconds) {
        this.rememberMeSeconds = rememberMeSeconds;
    }

    public String getSignUpUrl() {
        return signUpUrl;
    }

    public void setSignUpUrl(String signUpUrl) {
        this.signUpUrl = signUpUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getSignInSuccessUrl() {
        return signInSuccessUrl;
    }

    public void setSignInSuccessUrl(String signInSuccessUrl) {
        this.signInSuccessUrl = signInSuccessUrl;
    }
}

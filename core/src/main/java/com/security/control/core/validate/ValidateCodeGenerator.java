package com.security.control.core.validate;

import com.security.control.core.validate.support.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 14:29
 * ---验证码生成器接口
 */
public interface ValidateCodeGenerator {

    //生成图片验证码
    ValidateCode generate(ServletWebRequest request);

}

package com.security.control.core.social.support;

import org.springframework.social.security.SocialAuthenticationFilter;

/**
 * @author imoot@gamil.com
 * @date 2019/1/16 0016 14:17
 * SocialAuthenticationFilter后的处理器，用于在不同环境下个性化社交登录配置
 */
public interface SocialAuthenticationFilterPostProcessor {

    void process(SocialAuthenticationFilter socialAuthenticationFilter);

}

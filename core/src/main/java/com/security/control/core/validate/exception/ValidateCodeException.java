package com.security.control.core.validate.exception;


import org.springframework.security.core.AuthenticationException;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 10:47
 * ---验证码异常类
 */
public class ValidateCodeException extends AuthenticationException {

    public ValidateCodeException(String msg) {
        super(msg);
    }
}

package com.security.control.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2018/12/21 0021 17:40
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class SmsValidateCodeProperties extends ValidateCodeProperties {
}

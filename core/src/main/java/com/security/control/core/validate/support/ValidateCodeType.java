package com.security.control.core.validate.support;

import com.security.control.core.support.SecurityConstants;

/**
 * @author imoot@gamil.com
 * @date 2018/12/21 0021 18:04
 * ---校验码类型
 */
public enum ValidateCodeType {

    //短信校验码
    SMS {
        @Override
        public String getParamNameOnValidate() {
            return SecurityConstants.DEFAULT_PARAMETER_NAME_CODE_SMS;
        }
    },
    //图片验证码
    IMAGE {
        @Override
        public String getParamNameOnValidate() {
            return SecurityConstants.DEFAULT_PARAMETER_NAME_CODE_IMAGE;
        }
    };

    //校验时从请求中获取的参数的名字
    public abstract String getParamNameOnValidate();

}

package com.security.control.core.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2018/12/18 0018 10:03
 * ---yml/properties文件配置汇总类
 */
@Component
@ConfigurationProperties
public class SecurityProperties {

    @Autowired
    private QQSocialProperties qqSocialProperties;

    @Autowired
    private WxSocialProperties wxSocialProperties;

    @Autowired
    private BrowserProperties browserProperties;

    @Autowired
    private SessionProperties sessionProperties;

    @Autowired
    private ImageValidateCodeProperties imageValidateCodeProperties;

    @Autowired
    private SmsValidateCodeProperties smsValidateCodeProperties;

    public QQSocialProperties getQqSocialProperties() {
        return qqSocialProperties;
    }

    public BrowserProperties getBrowserProperties() {
        return browserProperties;
    }

    public ImageValidateCodeProperties getImageValidateCodeProperties() {
        return imageValidateCodeProperties;
    }

    public SmsValidateCodeProperties getSmsValidateCodeProperties() {
        return smsValidateCodeProperties;
    }

    public WxSocialProperties getWxSocialProperties() {
        return wxSocialProperties;
    }

    public SessionProperties getSessionProperties() {
        return sessionProperties;
    }

}

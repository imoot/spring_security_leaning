package com.security.control.core.validate.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 15:42
 * ---默认短信验证码发送器
 */
public class DefaultSmsValidateCodeSender implements SmsValidateCodeSender {

    private Logger log = LoggerFactory.getLogger(DefaultSmsValidateCodeSender.class);

    /**
     * @param mobile
     * @param code
     * 发送验证码
     */
    @Override
    public void send(String mobile, String code) {
        //正常开发环境中按照要求配置服务商短信发送sdk
        log.warn("请配置真实的短信验证码发送器{SmsValidateCodeSender}");
        log.info("向" + mobile + "发送短信验证码" + code);
    }
}

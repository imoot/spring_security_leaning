package com.security.control.core.social.wx.api;

/**
 * @author imoot@gamil.com
 * @date 2019/1/3 0003 15:04
 * ----wx相关操作接口
 */
public interface Wx {

    WxUserInfo getUserInfo(String openId);

}

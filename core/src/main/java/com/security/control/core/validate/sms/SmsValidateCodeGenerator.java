package com.security.control.core.validate.sms;

import com.security.control.core.properties.SecurityProperties;
import com.security.control.core.utils.RandomUtils;
import com.security.control.core.validate.support.ValidateCode;
import com.security.control.core.validate.ValidateCodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author imoot@gamil.com
 * @date 2018/12/23 0023 10:28
 * ---短信验证码生成器
 */
@Component("smsValidateCodeGenerator")
public class SmsValidateCodeGenerator implements ValidateCodeGenerator {

    @Autowired
    private SecurityProperties securityProperties;

    /**
     * @param request
     * @return ValidateCode
     * 生成短信验证码
     */
    @Override
    public ValidateCode generate(ServletWebRequest request) {
        String code = RandomUtils.randomNumeric(securityProperties.getSmsValidateCodeProperties().getLength());
        return new ValidateCode(code, securityProperties.getSmsValidateCodeProperties().getExpireIn());
    }

}

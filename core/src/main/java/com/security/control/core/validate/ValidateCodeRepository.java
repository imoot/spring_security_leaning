package com.security.control.core.validate;

import com.security.control.core.validate.support.ValidateCode;
import com.security.control.core.validate.support.ValidateCodeType;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author imoot@gamil.com
 * @date 2018/12/23 0023 9:25
 * ---验证码存取器（一般存放在session或者redis中）
 */
public interface ValidateCodeRepository {

    /**
     * @param request
     * @param code
     * @param validateCodeType
     *保存验证码
     */
    void save(ServletWebRequest request, ValidateCode code, ValidateCodeType validateCodeType);

    /**
     * @param request
     * @param validateCodeType
     * @return ValidateCode
     * 获取验证码
     */
    ValidateCode get(ServletWebRequest request, ValidateCodeType validateCodeType);

    /**
     * @param request
     * @param codeType
     * 移除验证码
     */
    void remove(ServletWebRequest request, ValidateCodeType codeType);
}

package com.security.control.core.support;

/**
 * @author imoot@gamil.com
 * @date 2018/12/18 0018 15:16
 * 权限认证成功后的响应方式
 */
public enum AuthenticateResponseType {

    REDIRECT,
    JSON

}

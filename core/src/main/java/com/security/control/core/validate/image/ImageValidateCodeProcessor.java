package com.security.control.core.validate.image;

import com.security.control.core.validate.impl.AbstractValidateCodeProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import javax.imageio.ImageIO;

/**
 * @author imoot@gamil.com
 * @date 2018/12/23 0023 10:38
 * ---图形验证码处理器
 */
@Component("imageValidateCodeProcessor")
public class ImageValidateCodeProcessor extends AbstractValidateCodeProcessor<ImageValidateCode> {

    /**
     * @param request
     * @param imageValidateCode
     * @throws Exception
     * 把图形验证码写入到响应中
     */
    @Override
    protected void send(ServletWebRequest request, ImageValidateCode imageValidateCode) throws Exception {
        ImageIO.write(imageValidateCode.getImage(), "JPEG", request.getResponse().getOutputStream());
    }
}

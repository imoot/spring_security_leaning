package com.security.control.core.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/8 0008 11:08
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class WxSocialProperties extends SocialProperties {

    @Value("${com.security.control.social.wx.appId}")
    private String appId;

    @Value("${com.security.control.social.wx.appSecret}")
    private String appSecret;

    @Value("${com.security.control.social.wx.providerId}")
    private String providerId = "wx";

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

}

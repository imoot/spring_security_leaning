package com.security.control.core.properties;

import com.security.control.core.support.SecurityConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/9 0009 14:51
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class SessionProperties {

    //同一个用户在系统中的最大session数量
    @Value("${com.security.control.browser.session.maximumSessions}")
    private int maximumSessions = 1;

    //session失效时跳转的地址
    @Value("${com.security.control.browser.session.invalidSessionUrl}")
    private String invalidSessionUrl = SecurityConstants.DEFAULT_SESSION_INVALID_URL;

    //达到最大session时是否阻止新的登录请求，默认为false（不阻止，新的登录会让老的登录失效）
    @Value("${com.security.control.browser.session.maxSessionsPreventsLogin}")
    private boolean maxSessionsPreventsLogin = false;

    public int getMaximumSessions() {
        return maximumSessions;
    }

    public void setMaximumSessions(int maximumSessions) {
        this.maximumSessions = maximumSessions;
    }

    public String getInvalidSessionUrl() {
        return invalidSessionUrl;
    }

    public void setInvalidSessionUrl(String invalidSessionUrl) {
        this.invalidSessionUrl = invalidSessionUrl;
    }

    public boolean getMaxSessionsPreventsLogin() {
        return maxSessionsPreventsLogin;
    }

    public void setMaxSessionsPreventsLogin(boolean maxSessionsPreventsLogin) {
        this.maxSessionsPreventsLogin = maxSessionsPreventsLogin;
    }
}

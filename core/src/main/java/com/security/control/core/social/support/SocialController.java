package com.security.control.core.social.support;

import org.springframework.social.connect.Connection;

/**
 * @author imoot@gamil.com
 * @date 2019/1/16 0016 14:28
 * 构建社交登陆用户信息抽象类
 */
public abstract class SocialController {

    /**
     * @param connection
     * @return SocialUserInfo
     * 根据Connection中的信息来构建SocialUserInfo
     */
    protected SocialUserInfo buildSocialUserInfo(Connection<?> connection){
        SocialUserInfo userInfo = new SocialUserInfo();
        userInfo.setProviderId(connection.getKey().getProviderId());
        userInfo.setProviderUserId(connection.getKey().getProviderUserId());
        userInfo.setNickname(connection.getDisplayName());
        userInfo.setHeadimg(connection.getImageUrl());
        return userInfo;
    }

}

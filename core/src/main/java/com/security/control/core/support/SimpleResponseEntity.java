package com.security.control.core.support;

/**
 * @author imoot@gamil.com
 * @date 2018/12/18 0018 9:47
 */
public class SimpleResponseEntity {

    private Object content;

    public SimpleResponseEntity(Object content) {
        this.content = content;
    }

    public SimpleResponseEntity() {
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}

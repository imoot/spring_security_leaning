package com.security.control.core.support;

import java.util.UUID;

/**
 * other:HG
 * date:2018-7-19
 * UUID生成器
 */
public class UUIDCreaterUtils {

    //获取uuid;
    public static String uuidCreat(){
        return (UUID.randomUUID().toString().replaceAll("-","")).toLowerCase();
    }
}

package com.security.control.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author imoot@gamil.com
 * @date 2019/1/16 0016 10:07
 * 使用ConfigurationProperties注解读取yml时要注意：
 * ---最好使用Component把设置为系统组件
 * ---使用注解中的value值声明对应的properties范围
 * ---如果使用数组或者集合必须要new出对象
 */
@Component
@ConfigurationProperties("com.security.control")
public class OAuth2Properties {

    private String signingKey = "test";

    private List<OAuth2ClientProperties> clients = new ArrayList<>();

    public List<OAuth2ClientProperties> getClients() {
        return this.clients;
    }

    public String getSigningKey() {
        return signingKey;
    }

    public void setSigningKey(String signingKey) {
        this.signingKey = signingKey;
    }
}

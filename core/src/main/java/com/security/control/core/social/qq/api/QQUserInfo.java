package com.security.control.core.social.qq.api;

/**
 * @author imoot@gamil.com
 * @date 2019/1/3 0003 15:03
 * -----qq用户信息类
 */
public class QQUserInfo {

    private String ret;//返回码
    private String msg;//如果返回码小于0，会有相应的错误信息提示，返回数据全部用UTF-8编码
    private String nickname;//用户在QQ空间的昵称
    private String province;//省（直辖市）
    private String city;//市（直辖市区）
    private String year;//出生年月
    private String constellation;//？
    private String figureurl;//大小为30*30像素的QQ空间头像url
    private String figureurl_1;//大小为50*50像素的QQ空间头像url
    private String figureurl_2;//大小为100*100像素的QQ空间头像url
    private String figureurl_qq_1;//大小为40*40像素的QQ头像url
    private String figureurl_qq_2;//大小为100*100像素的QQ头像url
    private String gender;//性别（如果获取不到默认为男）
    private String is_yellow_vip;//标识用户是否为黄钻（0：不是；1：是）
    private String vip;//标识用户是否为vip（0：不是；1：是）
    private String yellow_vip_level;//黄钻等级
    private String level;//用户等级
    private String is_yellow_year_vip;//标识用户是否为年费黄钻（0：不是；1：是）
    private String is_lost;//？
    private String openId;//用户的openId

    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public String getFigureurl() {
        return figureurl;
    }

    public void setFigureurl(String figureurl) {
        this.figureurl = figureurl;
    }

    public String getFigureurl_1() {
        return figureurl_1;
    }

    public void setFigureurl_1(String figureurl_1) {
        this.figureurl_1 = figureurl_1;
    }

    public String getFigureurl_2() {
        return figureurl_2;
    }

    public void setFigureurl_2(String figureurl_2) {
        this.figureurl_2 = figureurl_2;
    }

    public String getFigureurl_qq_1() {
        return figureurl_qq_1;
    }

    public void setFigureurl_qq_1(String figureurl_qq_1) {
        this.figureurl_qq_1 = figureurl_qq_1;
    }

    public String getFigureurl_qq_2() {
        return figureurl_qq_2;
    }

    public void setFigureurl_qq_2(String figureurl_qq_2) {
        this.figureurl_qq_2 = figureurl_qq_2;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIs_yellow_vip() {
        return is_yellow_vip;
    }

    public void setIs_yellow_vip(String is_yellow_vip) {
        this.is_yellow_vip = is_yellow_vip;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getYellow_vip_level() {
        return yellow_vip_level;
    }

    public void setYellow_vip_level(String yellow_vip_level) {
        this.yellow_vip_level = yellow_vip_level;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getIs_yellow_year_vip() {
        return is_yellow_year_vip;
    }

    public void setIs_yellow_year_vip(String is_yellow_year_vip) {
        this.is_yellow_year_vip = is_yellow_year_vip;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getIs_lost() {
        return is_lost;
    }

    public void setIs_lost(String is_lost) {
        this.is_lost = is_lost;
    }
}

package com.security.control.core.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;

/**
 * @author imoot@gamil.com
 * @date 2019/1/14 0014 17:46
 * 默认的SocialUserDetailsService实现
 * ---不做任何配置，只在控制台打印一句日志并抛出异常，提醒业务系统自己配置SocialUserDetailsService
 */
public class DefaultSocialUserDetailsService implements SocialUserDetailsService {

    private Logger log = LoggerFactory.getLogger(DefaultSocialUserDetailsService.class);

    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        log.warn("请配置SocialUserDetailsService接口的实现");
        throw new UsernameNotFoundException(userId);
    }
}

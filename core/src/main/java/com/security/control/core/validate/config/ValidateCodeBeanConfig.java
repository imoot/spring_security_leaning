package com.security.control.core.validate.config;

import com.security.control.core.properties.SecurityProperties;
import com.security.control.core.validate.ValidateCodeGenerator;
import com.security.control.core.validate.image.ImageValidateCodeGenerator;
import com.security.control.core.validate.sms.DefaultSmsValidateCodeSender;
import com.security.control.core.validate.sms.SmsValidateCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 14:36
 * ---把所有的默认的验证码生成类声明并注入bean
 * 配置在这里的bean，业务系统都可以通过声明同类型或者同名的bean来覆盖安全模块默认的配置
 */
@Configuration
public class ValidateCodeBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    //声明并注入默认的图片验证码生成类
    //注意：bean的名字与方法名相同
    @Bean
    @ConditionalOnMissingBean(name = "imageValidateCodeGenerator")//如果在spring容器中找不到imageValidateCodeGenerator这个bean，则用此处声明的bean；反之，则用spring容器中的bean
    public ValidateCodeGenerator imageValidateCodeGenerator() {
        ImageValidateCodeGenerator codeGenerator = new ImageValidateCodeGenerator();
        codeGenerator.setSecurityProperties(securityProperties);
        return codeGenerator;
    }

    //声明并注入默认的短信验证码生成类
    @Bean
    //@ConditionalOnMissingBean(name = "smsValidateCodeSender")//如果在spring容器中找不到smsValidateCodeGenerator这个bean，则用此处声明的bean；反之，则用spring容器中的bean
    @ConditionalOnMissingBean(SmsValidateCodeSender.class)//如果在spring容器中找不到SmsCodeSender的实现bean，则用此处声明的bean；反之，则用spring容器中的实现bean
    public SmsValidateCodeSender smsValidateCodeSender() {
        return new DefaultSmsValidateCodeSender();
    }

}

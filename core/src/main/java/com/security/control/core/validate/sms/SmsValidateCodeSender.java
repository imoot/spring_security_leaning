package com.security.control.core.validate.sms;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 15:41
 * ---发送短信验证码接口
 */
public interface SmsValidateCodeSender {

    /**
     * @param mobile
     * @param code
     * 验证码发送
     */
    void send(String mobile, String code);

}

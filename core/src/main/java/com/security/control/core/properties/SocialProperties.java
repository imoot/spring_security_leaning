package com.security.control.core.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author imoot@gamil.com
 * @date 2019/1/7 0007 10:15
 */
@Configuration
@ConfigurationProperties(prefix = "com.security.control")
public class SocialProperties {

    @Value("${com.security.control.social.filterProcessesUrl}")
    private String filterProcessesUrl = "/auth";

    public String getFilterProcessesUrl() {
        return filterProcessesUrl;
    }

    public void setFilterProcessesUrl(String filterProcessesUrl) {
        this.filterProcessesUrl = filterProcessesUrl;
    }
}

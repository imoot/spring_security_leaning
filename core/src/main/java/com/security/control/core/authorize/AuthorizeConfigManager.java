package com.security.control.core.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * @author imoot@gamil.com
 * @date 2019/1/15 0015 9:54
 * 授权信息管理器（用于收集系统汇总所有的AuthorizeConfigProvider并加载其配置）
 */
public interface AuthorizeConfigManager {

    /**
     * @param config
     */
    void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);

}

package com.security.control.core.social.view;

import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author imoot@gamil.com
 * @date 2019/1/8 0008 17:00
 * 绑定结果视图
 */
public class MyConnectView extends AbstractView {

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        //如果model中有connection，则为绑定请求；如果没有，则为解绑请求
        if(null != model.get("connections")) {
            response.getWriter().write("<h3>绑定成功</h3>");
        }else {
            response.getWriter().write("<h3>解绑成功</h3>");
        }
    }

}

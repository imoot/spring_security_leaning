package com.security.control.browser.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * @author imoot@gamil.com
 * @date 2019/1/16 0016 17:14
 */
public interface BrowserSecurityConfigCallback {

    void config(HttpSecurity httpSecurity);

}

package com.security.control.browser.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author imoot@gamil.com
 * @date 2018/12/26 0026 16:05
 */
@Controller
public class HomeController {

    @GetMapping("/")
    public String home(){
        return "index";
    }

}

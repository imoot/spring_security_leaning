package com.security.control.browser.session;

import com.security.control.core.properties.SecurityProperties;
import org.springframework.security.web.session.InvalidSessionStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author imoot@gamil.com
 * @date 2019/1/9 0009 15:44
 * ----session失效时的默认处理类
 */
public class MyInvalidSessionStrategy extends AbstractSessionStrategy implements InvalidSessionStrategy {

    public MyInvalidSessionStrategy(SecurityProperties securityProperties) {
        super(securityProperties);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * session失效时的处理方法
     */
    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException {
        onSessionInvalid(request,response);
    }
}

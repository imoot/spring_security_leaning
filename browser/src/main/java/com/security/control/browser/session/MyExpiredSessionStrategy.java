package com.security.control.browser.session;

import com.security.control.core.properties.SecurityProperties;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author imoot@gamil.com
 * @date 2019/1/9 0009 14:29
 * ----并发登录导致session失效时默认的处理类
 */
public class MyExpiredSessionStrategy extends AbstractSessionStrategy implements SessionInformationExpiredStrategy {

    public MyExpiredSessionStrategy(SecurityProperties securityProperties) {
        super(securityProperties);
    }

    /**
     * @param event
     * @throws IOException
     * 并发登录导致session失效时的处理方法
     */
    @Override
    public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException {
        onSessionInvalid(event.getRequest(),event.getResponse());
    }

    /**
     * @return boolean
     * session失效是否是并发导致的
     */
    @Override
    protected boolean isConcurrency() {
        return true;
    }
}

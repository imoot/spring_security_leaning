package com.security.control.browser.authorize;

import com.security.control.core.authorize.AuthorizeConfigProvider;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/16 0016 16:32
 * 浏览器环境默认的授权配置，对常见的静态资源，如：js、css、图片等不验证身份
 */
@Component
@Order(Integer.MIN_VALUE)
public class BrowserAuthorizeConfigProvider implements AuthorizeConfigProvider {

    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        config.antMatchers(HttpMethod.GET, "/**/*.js", "/**/*.css", "/**/*.jpg", "/**/*.png", "/**/*.gif").permitAll();
        return false;
    }
}

package com.security.control.browser.logout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.security.control.core.support.SimpleResponseEntity;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author imoot@gamil.com
 * @date 2019/1/10 0010 10:33
 * -----自定义退出成功处理器
 */
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    private Logger log = LoggerFactory.getLogger(MyLogoutSuccessHandler.class);

    private String logoutUrl;

    private ObjectMapper objectMapper = new ObjectMapper();

    public MyLogoutSuccessHandler(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    /**
     * @param request
     * @param response
     * @param authentication
     * @throws IOException
     * @throws ServletException
     * 自定义退出成功处理方法
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        log.info("退出成功");

        //判断退出登录的url是否为空，如果为空，返回json串；反之，重定向到url
        if (StringUtils.isNotBlank(logoutUrl)) {
            response.sendRedirect(logoutUrl);
        } else {
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(new SimpleResponseEntity("退出成功")));
        }

    }
}

package com.security.control.code;

import com.security.control.core.validate.image.ImageValidateCode;
import com.security.control.core.validate.ValidateCodeGenerator;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author imoot@gamil.com
 * @date 2018/12/19 0019 14:44
 * ---可以覆盖掉默认的图片验证码生成类
 */
//@Component("imageValidateCodeGenerator")
public class DemoImageCodeGenerator implements ValidateCodeGenerator {

    @Override
    public ImageValidateCode generate(ServletWebRequest request) {
        System.out.println("更高级的图形验证码生成器");
        return null;
    }
}

package com.security.control.validate.impl;

import com.security.control.service.TestService;
import com.security.control.validate.MyCheck;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 17:59
 * --自定义注解实现类
 * --实现了ConstraintValidator<对应哪个注解,可以在什么类型上使用>接口后不需要用@Component声明
 */
public class MyCheckImpl implements ConstraintValidator<MyCheck,Object> {

    @Autowired
    private TestService testService;//注解实现类可以使用自定义的service

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        testService.show(value.toString());
        System.out.println(value.toString());
        return false;
    }

    @Override
    public void initialize(MyCheck constraintAnnotation) {
        System.out.println("my check init");
    }
}

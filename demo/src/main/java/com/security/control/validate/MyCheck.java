package com.security.control.validate;

import com.security.control.validate.impl.MyCheckImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 17:57
 */
@Target({ElementType.METHOD,ElementType.FIELD})//声明可以在什么上使用（方法/属性）
@Retention(RetentionPolicy.RUNTIME)//什么时候校验
@Constraint(validatedBy = MyCheckImpl.class)//注解实现类
public @interface MyCheck {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

package com.security.control.web.async.holder;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 17:04
 * --存放消息
 */
@Component
public class DeferredResultsHolder {

    private Map<String,DeferredResult<String>> map = new HashMap<>();

    public Map<String, DeferredResult<String>> getMap() {
        return map;
    }

    public void setMap(Map<String, DeferredResult<String>> map) {
        this.map = map;
    }
}

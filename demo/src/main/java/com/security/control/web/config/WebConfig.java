package com.security.control.web.config;

import com.security.control.web.filter.TimeFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 11:50
 * --web配置类（作用和web.xml近似）
 * --注意：在springboot2.0以上WebMvcConfigurerAdapter 已过时，用WebMvcConfigurationSupport来替代
 * --Filter、Interceptor、ControllerAdvice、Aspect、Controller的先后顺序
 * 入：Filter>Interceptor>ControllerAdvice>Aspect>Controller
 * 出现异常：Controller>Aspect>ControllerAdvice>Interceptor>Filter
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    //注入拦截器
    //@Autowired
    //private TimeInterceptor timeInterceptor;

    //Filter注册
    //@Bean
    public FilterRegistrationBean timeFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        TimeFilter timeFilter = new TimeFilter();
        registrationBean.setFilter(timeFilter);//把自定义Filter添加到项目的过滤中

        //限制过滤的url列表
        List<String> urlList = new ArrayList<>();
        urlList.add("/user");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;
    }


    //添加Interceptor方法
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(timeInterceptor);
    }

    //在异步情况下需要这样注册Interceptor
    @Override
    protected void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        //configurer.registerCallableInterceptors(new CallableProcessingInterceptor() {});//在使用Runnable情况下（需要自己实现runnable）
        //configurer.registerDeferredResultInterceptors(new DeferredResultProcessingInterceptor() {});//在使用Deferred情况下（需要自己实现deferred）
        //configurer.setTaskExecutor();//配置相对应的的异步线程池（需要自己实现线程池）
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        //添加swagger静态文件映射
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:META-INF/resources/webjars/");
        //添加静态资源文件映射
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
        super.addResourceHandlers(registry);
    }
}

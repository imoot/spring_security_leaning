package com.security.control.web.async.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 17:01
 * --模拟消息队列
 */
@Component
public class MockQueue {

    private static final Logger log = LoggerFactory.getLogger(MockQueue.class);

    private String placeOrder;
    private String completeOrder;

    public String getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder) {
        new Thread(()->{
            log.info("接到下单请求：" + placeOrder);
            try {
                Thread.sleep(1000);
            }catch (Exception e){
                e.printStackTrace();
            }
            this.placeOrder = placeOrder;
            log.info("下单请求处理完毕：" + placeOrder);
        }).start();
    }

    public String getCompleteOrder() {
        return completeOrder;
    }

    public void setCompleteOrder(String completeOrder) {
        this.completeOrder = completeOrder;
    }
}

package com.security.control.web.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 14:52
 */
public class UserQueryCondition {

    @ApiModelProperty(value = "用户id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

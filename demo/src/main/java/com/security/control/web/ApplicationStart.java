package com.security.control.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author imoot@gamil.com
 * @date 2018/8/27 17:23
 */
@SpringBootApplication
@ComponentScan("com.security.control")
//@EnableAutoConfiguration
//@RestController
public class ApplicationStart extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationStart.class,args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApplicationStart.class);
    }

    //@GetMapping("/hello")
    public String hello(){
        return "hello";
    }
}

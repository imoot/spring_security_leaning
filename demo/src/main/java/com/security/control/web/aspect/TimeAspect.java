package com.security.control.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 15:35
 * --自定义切片类
 *      切入点（注解）：
 *          在哪些方法上起作用
 *          在什么时候起作用
 *      增强（方法）：
 *          执行的业务逻辑
 * --可以拿到要执行的方法信息以及传递到方法中的形参的值，拿不到原始的http请求响应信息
 */
/*@Aspect
@Component*/
public class TimeAspect {

    //@Before()方法执行前
    //@After()方法执行完成后
    //@AfterThrowing方法抛出异常后
    @Around("execution(* com.security.web.controller.UserController.*(..))")//覆盖前三种注解，一般使用Around
    public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("time aspect start");
        Object[] args = pjp.getArgs();
        System.out.println("time aspect print args--------------");
        for (Object arg : args) {
            System.out.println("arg is " + arg);
        }
        System.out.println("---------------------------------------");
        long start = new Date().getTime();
        Object result = pjp.proceed();//等同于chain.doFilter(request, response)
        System.out.println("time aspect use time：" + (new Date().getTime() - start));
        System.out.println("time aspect end");

        return result;
    }

}

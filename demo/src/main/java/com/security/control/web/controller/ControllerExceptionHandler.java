package com.security.control.web.controller;

import com.security.control.exception.UserNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 11:32
 * --controller错误处理器（声明异常怎么处理 交给哪个类处理）
 * --声明了@ControllerAdvice的异常处理类会在自定义Interceptor中的afterCompletion()方法之前执行，所以afterCompletion()拿不到Exception
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(UserNotExistException.class)//异常交给哪个类来处理
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)//服务器响应状态
    public Map<String,Object> handlerUserNotExistException(UserNotExistException ex){
        Map<String,Object> errors = new HashMap<>();
        errors.put("id",ex.getId());
        errors.put("message",ex.getMessage());
        return errors;
    }

}

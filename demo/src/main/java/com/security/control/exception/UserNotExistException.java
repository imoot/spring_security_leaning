package com.security.control.exception;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 11:26
 */
public class UserNotExistException extends RuntimeException {

    private String id;

    public UserNotExistException(String id) {
        super("user not exist");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.security.control.security;

import com.security.control.core.authorize.AuthorizeConfigProvider;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * @author imoot@gamil.com
 * @date 2019/1/25 0025 14:14
 * demo项目中的权限配置提供器
 */
@Component
@Order(100)
public class DemoAuthorizeConfigProvider implements AuthorizeConfigProvider {

    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        //config.antMatchers(HttpMethod.GET,"/user/*").hasRole("ADMIN");//用get方式访问"/user/*"需要指定的角色
        //config.antMatchers(HttpMethod.GET,"/user/*").access("hasRole('ADMIN') and hasIpAddress('127.0.0.1')");//如果需要连续使用权限表达式，不能用链式编程，应该调用access()传入权限表达式的字符串
        config.anyRequest().access("@rbacService.hasPermission(request,authentication)");//如果要使用自定义的权限校验，使用access()传入自定义的校验组件以做调用
        return false;
    }
}

package com.security.control.entity;

/**
 * @author imoot@gamil.com
 * @date 2018/12/15 0015 16:13
 */
public class FileInfo {

    private String path;



    public FileInfo(String path) {
        this.path = path;
    }

    public FileInfo() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

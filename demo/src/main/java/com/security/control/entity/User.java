package com.security.control.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.security.control.validate.MyCheck;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.util.Date;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 14:13
 * <p>
 * ----@JsonView限制属性在什么状态下展示
 *
 */
public class User {

    public interface UserSimpleView {
    }

    public interface UserDetailView extends UserSimpleView {
    }

    private String id;
    @MyCheck(message = "我是个测试的")
    private String username;
    //@NotBlank限制属性不为空，每个校验注解都有message属性，用来自定义错误提示信息
    @NotBlank(message = "密码not空")
    private String password;
    @Past
    private Date birthday;

    @JsonView(UserSimpleView.class)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonView(UserSimpleView.class)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonView(UserDetailView.class)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonView(UserSimpleView.class)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}

package com.security.control.service;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 18:02
 */
public interface TestService {

    String show(String name);

}

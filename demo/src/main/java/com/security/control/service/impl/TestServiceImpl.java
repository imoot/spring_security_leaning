package com.security.control.service.impl;

import com.security.control.service.TestService;
import org.springframework.stereotype.Service;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 18:02
 */
@Service
public class TestServiceImpl implements TestService {

    @Override
    public String show(String name) {
        System.out.println("my test service");
        return name;
    }
}

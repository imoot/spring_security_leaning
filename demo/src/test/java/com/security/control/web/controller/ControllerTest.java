package com.security.control.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author imoot@gamil.com
 * @date 2018/12/14 0014 16:13
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ApplicationStart.class)
public class ControllerTest {

    /*private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext applicationContext;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void querySuccess() throws Exception {
        String result = mockMvc.perform(
                MockMvcRequestBuilders.get("/user")
                        .param("userId", "")
                        .param("size", "10")
                        .param("page", "5")
                        .param("sort", "userId,desc")
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        //.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));
        System.out.println(result);
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void getInfoSuccess() throws Exception {
        String result = mockMvc.perform(
                MockMvcRequestBuilders.get("/user/123")
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("123"))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void getInfoForRegularCheck() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/user/ttt")
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void createSuccess() throws Exception {
        Date date = new Date();
        System.out.println(date.getTime());
        //传递时间类型的数据是最好用时间戳
        String content = "{\"username\":\"test\",\"password\":\"\",\"birthday\":\"" + date.getTime() + "\"}";
        String result = mockMvc.perform(
                MockMvcRequestBuilders.post("/user")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(content))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("123"))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void updateSuccess() throws Exception {
        Date date = new Date(LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        System.out.println(date.getTime());
        String content = "{\"id\":\"123\",\"username\":\"test\",\"password\":\"\",\"birthday\":\"" + date.getTime() + "\"}";
        String result = mockMvc.perform(
                MockMvcRequestBuilders.put("/user/123")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(content))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("123"))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void deleteSuccess() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/user/123")
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", password = "123456")
    public void uploadSuccess() throws Exception {
        String result = mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/file")
                        .file(new MockMultipartFile("file", "test.txt", "multipart/form-data", "hello upload".getBytes("UTF-8"))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }*/
}

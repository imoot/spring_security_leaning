package com.security.control.wiremock;

import com.github.tomakehurst.wiremock.client.WireMock;

/**
 * @author imoot@gamil.com
 * @date 2018/12/17 0017 11:20
 */
public class WireServer {

    public static void main(String[] args) {
        WireMock.configureFor(8062);
        WireMock.removeAllMappings();

        mock("/order/1","{\"id\":1}");
    }

    public static void mock(String url, String result) {
        WireMock.stubFor(
                WireMock.get(WireMock.urlEqualTo(url))
                        .willReturn(WireMock.aResponse()
                                .withBody(result)
                                .withStatus(200)));
    }

}

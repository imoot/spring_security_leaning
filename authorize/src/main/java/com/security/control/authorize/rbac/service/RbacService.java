package com.security.control.authorize.rbac.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * @author imoot@gamil.com
 * @date 2019/1/25 0025 14:52
 */
public interface RbacService {

    boolean hasPermission(HttpServletRequest request, Authentication authentication);

}

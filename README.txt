SpringBoot2.x+Security

#项目中用到的说明
provider--------------------->服务提供商
        ：Authorization Server ----------->认证服务器
           Resource Server-------------->资源服务器
resource owner--------------->资源所有者
client---------------->第三方应用

OAuth2协议中的四种授权模式：
    authorization code                              ------->授权码模式
    implicit                                               ------->简化模式
    resource owner password credentials  ------->密码模式
    client credentials                                 ------->客户端模式

#备注：
此项目仅供学习使用